import jdk.jfr.ContentType;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.*;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.security.cert.CRL;
import java.util.*;

final class HttpRequest implements Runnable {

    final static String CRLF = "\r\n"; // Carriage Return and Line Feed
    Socket socket; // store a reference to the connection socket

    // Constructor
    public HttpRequest(Socket socket) throws Exception {
        this.socket = socket;
    }

    // Implemente the run() method of the Runnable Interface
    // cannot throws exception
    public void run(){

        try {
            processRequest();
        } catch (Exception e) {
            System.out.println("Error while Processing Request: " + e);
        }
    }

    private void processRequest() throws Exception {
        
        // Get a reference to the socket's input and output streams.
        InputStream is = socket.getInputStream();
        DataOutputStream dos = new DataOutputStream(socket.getOutputStream());

        // Set up input stream filters.
        InputStreamReader is_reader = new InputStreamReader(is); // OKOK
        BufferedReader br = new BufferedReader(is_reader);

        // Get the request line of the HTTP request message.
        String requestLine = br.readLine(); // we have the request line -> now we can get the headers
        
        // Display the request line.
        System.out.println();
        System.out.println("RequestLine: " + requestLine);

        // Get and display the header lines .
        // Since we don't know how many lines, lets make a loop
        String headerLine = null;
        while((headerLine = br.readLine()).length() != 0) { // queremos aprenas printar os headers na tela
            System.out.println(headerLine);
        }

        // Extract the filename from the request line.
        StringTokenizer tokens = new StringTokenizer(requestLine);
        tokens.nextToken(); // skip over the method, which should be 'GET'.
        String fileName = tokens.nextToken();

        // Prepend a "." so that file request is within the current directory.
        fileName = "." + fileName; // I choose the first chapter of Alice in Wonderland, since we used it in previous exercises.
        System.out.println();
        System.out.println("File Name:" + fileName); // we get the name of the file without the "/"

        // Open the requested file.
        // and verifying if it exists or not.
        FileInputStream fis = null;
        boolean fileExists = true;
        try {
            fis = new FileInputStream(fileName);
            System.out.println("FOUND YA!!!");
        } catch (FileNotFoundException e) {
            fileExists = false;
            System.out.println("NO FILE FOUND");
        }

        // Construct the response message.
        // according to the request.
        String statusLine = null;
        String contentTypeLine = null;
        String contentLengthLine = null;
        String entityBody = null;
        if(fileExists) {
            // Setting the status.
            statusLine = "HTTP/1.1 200 OK" + CRLF;
            contentTypeLine = "Content-Type: " + contentType(fileName) + CRLF;

            // Now writing in the socket output.
            dos.writeBytes(statusLine); // Send the status line.
            dos.writeBytes(contentTypeLine); // Send the content type line.
            dos.writeBytes(CRLF); // Send a blank line to indicate the end of the header lines.

            // Reading the file.
            FileInputStream fileInputStream = new FileInputStream(fileName);
            byte[] buffer = new byte[1024];
            int bytes = 0;
            while ((bytes = fileInputStream.read(buffer)) != -1) {
                dos.write(buffer, 0, bytes);
            }
            //dos.writeBytes(text); // The body of my file;
        } else {
            statusLine = "HTTP/1.1 404 Not Found" + CRLF;
            contentTypeLine = CRLF;
            //entityBody = "<html><head><title>404 Not Found</title></head><body><h1>File Not Found</h1><p>The requested file was not found on this server. Make sure the file name is right, and have a nice day!</p></body></html>";
            entityBody = "<html>\n" +
                    "<body>\n" +
                    "\n" +
                    "<h1>404 Not Found</h1>\n" +
                    "<h2>Sorry, could not found your file :(</h2>\n" +
                    "<h3>Hope you have a nice day!</h3>\n" +
                    "<h4>:)</h4>\n" +
                    "<h5>:D</h5>\n" +
                    "<h6>^-^</h6>\n" +
                    "\n" +
                    "</body>\n" +
                    "</html>";


            // Now writing in the socket output.
            dos.writeBytes(statusLine); // Send the status line.
            dos.writeBytes(contentTypeLine); // Send the content type line.
            dos.writeBytes(CRLF); // Send a blank line to indicate the end of the header lines.
            dos.writeBytes(entityBody); // maybe
        }

        // Close streams and socket.
        dos.close();
        br.close();
        socket.close();

    }


    // Writes the requested file onto the socket's output stream, sending the file to the client.
    private static void sendBytes(FileInputStream fis, DataOutputStream os) throws Exception {

        // Construct a 1K buffer to hold bytes on their way to the socket.
        byte[] buffer = new byte[1024];
        int bytes = 0;

        // Copy requested file into the socket's output stream.
        while((bytes = fis.read(buffer)) != -1) { // -1 indicates the end of the file.
            os.write(buffer, 0, bytes); // 0 is the starting point in the array and bytes the number of bytes.
        }

    }

    // Determines the MIME type of a file.
    // Examine the extension of a file name and return a string that represents it's MIME type.
    // If the file extension is unknown we return the type: application/octet-stream
    private static String contentType(String fileName) {

        if(fileName.endsWith(".htm") || fileName.endsWith(".html")) {
            return "text/html";
        }
        if(fileName.endsWith(".txt")) {
            return "text/txt";
        }
        if(fileName.endsWith(".jpeg") || fileName.endsWith(".jpg")){
            return "image/jpeg";
        }
        return "application/octet-stream";
    }

}